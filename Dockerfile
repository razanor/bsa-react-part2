FROM node:8.7.0-alpine

LABEL maintainer="nazara.re22@gmail.com"

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm i

COPY . .

EXPOSE 3000

CMD ["npm", "start"]