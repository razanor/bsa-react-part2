import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import Chat from './containers/Chat/'

import 'semantic-ui-css/semantic.min.css';
import './styles/index.css';

import store from './store';

const target = document.getElementById('root');
render(<Provider store={store}>
    <Chat />
</Provider>, target);
