import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import chatReducer from './containers/Chat/reducer';
import { composeWithDevTools } from 'redux-devtools-extension';

const initialState = {};

const middlewares = [thunk];

const composedEnhancers = compose(
    applyMiddleware(...middlewares)
);

const reducers = {
    messages: chatReducer
};

const rootReducer = combineReducers({
    ...reducers
});

const store = createStore(
    rootReducer, 
    initialState,
    composeWithDevTools(composedEnhancers)
);

export default store;