import React from 'react';
import PropTypes from 'prop-types';
import Message from '../../components/Message';
import { Segment } from 'semantic-ui-react';


export const MessageList = ( { messages }) => (
    <Segment className="messagesWrapper">
        {messages.map(message => (
            <Message key={message.id} 
                message={message}
            />
        ))}
    </Segment>
);

MessageList.propTypes = {
    messages: PropTypes.array.isRequired
}

export default MessageList;