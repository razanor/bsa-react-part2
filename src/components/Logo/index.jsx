import React from 'react';
import { Image, Container } from 'semantic-ui-react';

export const Logo = () => (
    <Container>
        <Image size='small' src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ3A6CzwqxUzweNAv6CO8AumK3DYqSbTqDQVdOcixmDMxy46knZTA' />
    </Container>
);

export default Logo;