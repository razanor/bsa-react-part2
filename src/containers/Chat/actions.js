import {
    SET_IS_LOADING, 
    SET_ALL_MESSAGES,
    ADD_MESSAGE,
    EDIT_MESSAGE,
    DELETE_MESSAGE
} from './actionTypes';


const actionSetAllMessages = messages => ({
    type: SET_ALL_MESSAGES,
    messages
});

const actionSetIsLoading = isLoading => ({
    type: SET_IS_LOADING,
    isLoading
});

const actionAddMessage = message => ({
    type: ADD_MESSAGE,
    message
});

const actionDeleteMessage = id => ({
    type: DELETE_MESSAGE,
    id
});

const actionEditMessage = message => ({
    type: EDIT_MESSAGE,
    message
});

export const fetchMessages = () => dispatch => {
    dispatch(actionSetIsLoading(true));
    fetch('https://api.myjson.com/bins/1hiqin')
        .then(res => res.json())
        .then(messages => {
            dispatch(actionSetAllMessages(messages));
            dispatch(actionSetIsLoading(false));
        });
};

export const addMessage = newMessage => dispatch => {
    dispatch(actionAddMessage(newMessage));
};

export const deleteMessage = id => dispatch => {
    dispatch(actionDeleteMessage(id));
};

export const editMessage = message => dispatch => {
    dispatch(actionEditMessage(message));
};