import React from 'react';
import PropTypes from 'prop-types';
import Spinner from '../../components/Spinner';
import Logo from '../../components/Logo';
import Header from '../../components/Header';
import MessageList from '../../components/MessageList';
import MessageInput from '../../components/MessageInput';
import { connect } from 'react-redux';
import { fetchMessages } from './actions';
import moment from 'moment';

class Chat extends React.Component {

    componentDidMount() {
        this.props.fetchMessages();
    }

    render() {
        const { messages, isLoading } = this.props;
        const uniqueUsers = [...new Set(messages.map(m => m.user))].length;
        const allDates = messages.map(m => moment(m.created_at));
        const mostRecentDate = moment.max(allDates).fromNow();
        return (
            isLoading
                ? <Spinner />
                : (
                    <div className="wrapper">
                        <Logo />
                        <Header 
                            participants={uniqueUsers} 
                            messages={messages.length}
                            mostRecentDate={mostRecentDate} 
                        />
                        <MessageList messages={messages} />
                        <MessageInput messages={messages} />
                    </div>
                  )
        )
    }
}

Chat.propTypes = {
    fetchMessages: PropTypes.func.isRequired,
    messages: PropTypes.array.isRequired,
    isLoading: PropTypes.bool.isRequired
}

const mapStateToProps = state => ({
    messages: state.messages.messages,
    isLoading: state.messages.isLoading
});

export default connect(mapStateToProps, { fetchMessages } )(Chat);