import {
    SET_ALL_MESSAGES,
    SET_IS_LOADING,
    ADD_MESSAGE,
    EDIT_MESSAGE,
    DELETE_MESSAGE
} from './actionTypes';

const initialState = {
    isLoading: true,
    messages: [],
}

export default function(state = initialState, action) {
    switch (action.type) {
        case SET_IS_LOADING:
            return {
                ...state,
                isLoading: action.isLoading
            };
        case SET_ALL_MESSAGES:
            return {
                ...state,
                messages: action.messages
            };
        case ADD_MESSAGE:
            return {
                ...state,
                messages: [...state.messages, action.message]
            };
        case DELETE_MESSAGE:
            return {
                ...state,
                messages: state.messages.filter(m => m.id !== action.id)
            }
        case EDIT_MESSAGE:
            return {
                ...state,
                messages: state.messages.map(m => m.id === action.message.id ? action.message : m)
            }
        default:
            return state;
    }
}